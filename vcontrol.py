class GasBoiler:
    
    def __init__(self, port, deviceIdentifier):
        self.__port = port
        self.__deviceIdentifier = deviceIdentifier
        self.__serial = __import__('serial')
        
    def __del__(self):
        pass
    
    def getPort(self):
        return self.__port
    
    def getdeviceIdentifier(self):
        return self.__deviceIdentifier
        
    def connect(self):
        """establish connection to boiler
        
        returns true if successfully connected, otherwise false
        """
        
        #create serial port
        self.__ser = self.__serial.Serial(port=self.__port, baudrate=4800, bytesize=8, parity='E', stopbits=2, timeout=3, xonxoff=0, rtscts=0, dsrdtr=0, write_timeout=3)
        
        #send 0x04 to reset controller
        self.__ser.write(b'\x04')
        
        #send sync sequence if controller is reset
        x = self.__ser.read(1)
        if x == b'\x05':
            self.__ser.write(b'\x16\x00\x00')
        elif x == b'\x06':
            x = self.__ser.read(1)
            if x == b'\x05':
                self.__ser.write(b'\x16\x00\x00')
        
        if self.__ser.read(1) == b'\x06':
            print("Connected!")
            return True
        else:
            return False
    
    def disconnect(self):
        """clean disconnect from controller and close serial port
        """
        
        #send 0x04 to reset controller
        self.__ser.write(b'\x04')
        
        #successfully disconnected if controller is sending 0x05
        x = self.__ser.read(1)
        if x == b'\x05':
            self.__ser.close()
            print("Disconnected!")
        elif x == b'\x06':
            x = self.__ser.read(1)
            if x == b'\x05':
                self.__ser.close()
                print("Disconnected!")
 
    def calculateCheckSum(self, byteString):
        """calculate 2 byte checksum of byteString
        
        sum of all values
        """
        checksum = hex(sum(bytes.fromhex(byteString)) % 256)
        checksum = checksum[2:]
        checksum = checksum.zfill(2)
        
        return checksum.upper()
    
    def readAddress(self, address, bytes_received):
        """read the value of given address
        
        arguments:
        address -- address in hex where the value is stored
        bytes_received -- quantity of bytes to be written
        
        returns data in hex
        returns empty string if error
        """
        
        #convert to double-digit numer
        bytes_received = bytes_received.zfill(2)
        address = address.upper()
        
        #build complete string to send to controller
        requestString = "050001" + address + bytes_received
        
        #calculate checksum of string which is send to controller
        requestChecksum = self.calculateCheckSum(requestString)
        
        print("Request checksum:", requestChecksum)
        
        requestString = "41"+requestString+requestChecksum
        print("Request string:",requestString)
        
        #send request to controller
        self.__ser.write(bytes.fromhex(requestString))
        
        #read response from controller
        responseString = self.__ser.read((8+int(bytes_received)+1))
        
        responseString = responseString.hex().upper()
        
        print("Response string:", responseString)
        
        #calculate checksum of response string
        responseChecksum = self.calculateCheckSum(responseString[4:len(responseString)-2])
        
        print("Response checksum:", responseChecksum)
        
        #if checksum is valid and Message Identifier is 0x01, value valid return data
        if responseChecksum == responseString[len(responseString)-2:] and responseString[6:8] == "01":
            print("Checksum valid!")
            
            responseString = responseString[16:len(responseString)-2]
            
            print("Return data (HEX):", responseString)
            
            return responseString
        else:
            print("Checksum or response code invalid!")
            
            print("Return empty string")
            
            return ""
    
    def writeAddress(self, address, bytes_received, value):
        """set value of given address
        
        arguments:
        address -- address in hex of value to be set
        bytes_received -- quantity of bytes to be written
        value -- value of address to be set
        
        returns true if command was excecuted, otherwise false
        """
        
        #convert to double-digit number
        bytes_received = bytes_received.zfill(2)
        
        #convert value to valid bytes according to the number of bytes to be written
        value = value.zfill(int(bytes_received)*2)
        address = address.upper()
 
        #build complete string to send to controller
        requestString = "060002" + address + bytes_received + value
        
        #calculate checksum of string which is send to controller
        requestChecksum = self.calculateCheckSum(requestString)
        
        print("Request checksum:", requestChecksum)
        
        requestString = "41"+requestString+requestChecksum
        print("Request string:", requestString)
        
        #send request to controller
        self.__ser.write(bytes.fromhex(requestString))
        
        #read response from controller
        responseString = self.__ser.read(9)
        
        responseString = responseString.hex().upper()
        
        print("Response string:", responseString)
        
        #calculate checksum of response string
        responseChecksum = self.calculateCheckSum(responseString[4:len(responseString)-2])
        
        print("Response checksum:", responseChecksum)
        
        #if checksum is valid and Message Identifier is 0x01, command excecuted return true
        if responseChecksum == responseString[len(responseString)-2:] and responseString[6:8] == "01":
            print("Command excecuted!")
            return True
        else:
            print("Command failed!")
            return False

gasBoiler_1 = GasBoiler(port="/dev/serial0", deviceIdentifier="20C2")
gasBoiler_1.connect()
print(int.from_bytes(bytes.fromhex(gasBoiler_1.readAddress(address="5527", bytes_received="2")), byteorder='little') / 10)

#gasBoiler_1.writeAddress(address="2302", bytes_received="1", value="0")
gasBoiler_1.disconnect()
